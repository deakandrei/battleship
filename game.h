#ifndef BATTLESHIP_H
#define BATTLESHIP_H

typedef struct SDL_Surface SDL_Surface;

enum degrees_t {
	ROT_NORMAL = 0,
	ROT_90,	// all rotations are to the left
	ROT_180,
	ROT_270,
	ROT_NR
};

class Shape {
	SDL_Surface * rot[ROT_NR];
	int ref_count;
	public:
		Shape(const char * resource_file);
		SDL_Surface * get_shape(degrees_t degrees); // returns the surface rotated by the specified degrees
		void reference();
		void dereference();
		int get_ref_count();
		~Shape();
};

struct Point {
	int x, y;
	Point() = default;
	Point(int a, int b) { x = a; y = b; };
	friend Point operator-(Point a, Point b); 
	friend Point operator+(Point a, Point b);
	double norm();// { return sqrt(x * x + y * y); }
};

class WeaponSystem {
	private:
		int max_ammo_nr;
		int ammo_left;
		int range;
		int damage;
	public:
		WeaponSystem() {max_ammo_nr = ammo_left = 0; range = 4000; damage = 100; };
		WeaponSystem(int max_ammo) { max_ammo_nr = max_ammo; ammo_left = 0; range = 4000; }; // the range value is for rockets
		~WeaponSystem() = default;
		void reload(int nr);
		void set_range(int range);
		void set_max_ammo(int nr);
		void set_damage(int nr) { damage = nr; };
		int get_max_ammo() { return max_ammo_nr; };
		int get_ammo_left() { return ammo_left; };
		bool can_fire_at(Point from, Point loc);
		int fire_at(Point from, Point loc);
};

typedef struct {
	short fuel_cost;
	short ammo_cost;
	short health_gained;
} action_type_t;

enum {
	OPT_SHIELD,
	OPT_CANCEL,
	OPT_FIRE_CANON,
	OPT_FIRE_ROCKET,
	OPT_REPAIR,
	OPT_SELF_DESTRUCT,
	OPT_MOVE,
	OPT_NUMBER
};

class Battleship;

typedef struct {
	int type; // index in action_catalog
	Point loc;
	Battleship * ship;
} Action;

/* the order in this must match the order in the enum above */
extern const action_type_t action_catalog[];

#define MAX_TEAM_NR 2

class Battleship {
	protected:
		Point position;	/* the center of the sprite, on the map (in pixels) */
		degrees_t rotation;
		short fuel, max_fuel;
		short health, max_health;
		short team_nr;
		WeaponSystem weapon;
		Action action;
	public:
		Battleship() = default;
		virtual SDL_Rect get_occupied_area() = 0; // in pixel map coordinates
		virtual SDL_Surface * get_sprite() = 0;
		Point get_position() { return position; };
		int get_team_nr() { return team_nr; };
		virtual int get_ship_type() = 0;
		void set_rotation(degrees_t rot) { rotation = rot; };
		/* this returns a vector that you must NOT free() witch ends with -1 
		 * and contains all possible actions that this ship can do */
		virtual const int * get_possible_actions() = 0;
		virtual int get_max_range() = 0;
		int get_fuel_left() { return fuel; };
		int get_max_fuel() { return max_fuel; };
		int get_health() { return health; };
		int get_max_health() { return max_health; };
		int get_ammo_left() { return weapon.get_ammo_left(); };
		int get_max_ammo() { return weapon.get_max_ammo(); };
		bool can_fire_at(Point loc) { return weapon.can_fire_at(position, loc); };
		bool can_go_to(Point x);
		virtual void deal_damage(int amt) { health -= amt; };
		bool is_allive() { return health > 0; };
		virtual void do_turn();
		void move();
		virtual bool set_action(Action * action);
		bool clicked(int x, int y);// (x, y) are in map coordinates
		void print(int mapx, int mapy);
		virtual ~Battleship() = default;
};

enum {
	SH_BOAT,
	SH_SUBMARINE,
	SH_DESTROYER,
	SH_CRUISER,
	SH_TYPES
};

Battleship * spawn_ship(int type, Point pos, int team_nr);

class Team {
	Battleship ** fleet;
	short size;	// the size of the fleet vector
	short allive;	// how many ships are still allive
	short ship_type_nr[SH_TYPES];// a vector that memorises how many ships do we have left of each type
	public:
		Team(int team_nr, int ship_type[SH_TYPES]);
		~Team();
		Battleship * get_ship_at(int x, int y);// map coordinates
		void do_turn();
		void move();
		void print_fleet(int mapx, int mapy);
		int get_ship_type_nr(int type);
		bool is_allive() { return allive > 0; };
	private:
		void allocate_ships(int team_nr, int ship_type[SH_TYPES], Point * position);
};

extern SDL_Rect map_area;

#endif /* BATTLESHIP_H */
