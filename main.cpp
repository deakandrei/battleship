#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <game.h>
#include <io.h>

Point operator-(Point a, Point b) {
	return (Point){a.x - b.x, a.y - b.y}; 
}

Point operator+(Point a, Point b) {
	return (Point){a.x + b.x, a.y + b.y};
}

double Point::norm() {
	return sqrt(x*x + y*y);
}

draw_context_t ctx;
SDL_Rect map_area;

const int menu_color = 0xff00e400;
const int text_color = 0xffd7d7d7;
const int green_team_color = 0xff4ae221;
const int orange_team_color = 0xfffb7305;

static inline void putpixel(SDL_Surface * srf, int x, int y, int color) {
	int * pixel = (int*)srf->pixels;
	pixel += y * (srf->pitch>>2) + x;
	//*pixel = (color.r << 16) + (color.g << 8) + (color.b << 0);
	*pixel = color;
}

static inline int getpixel(SDL_Surface * srf, int x, int y) {
	return *((int*)srf->pixels + y * (srf->pitch>>2) + x);
}

SDL_Surface * get_surface(int width, int height) {
	SDL_Surface * ptr = SDL_CreateRGBSurface(0, width, height,
			ctx.srf->format->BitsPerPixel,
			ctx.srf->format->Rmask, ctx.srf->format->Gmask,
			ctx.srf->format->Bmask, ctx.srf->format->Amask);
	SDL_SetSurfaceBlendMode(ptr, SDL_BLENDMODE_NONE);
	return ptr;
}

// Draws the character on the screen.
void putch(SDL_Surface * srf, int ch, int x, int y) {
	int width, X = x * ctx.font.width + ctx.font.width, Y = y * ctx.font.height;
	register char * gylph = ctx.font.data + ch * ctx.font.charsize;//the gylph we want
	//assert(ch < ctx.font.length);//this shouldn't happen but better check...
	for(int i = 0; i < ctx.font.height; i++) {
		width = ctx.font.width;
		do {
			for(int j = 7; j >= 0 && width; j--, width--) {//print 8 pixels at a time
				if((1 << j) & *(gylph))
					putpixel(srf, X - width, Y + i, ctx.fore);
				else
					putpixel(srf, X - width, Y + i, ctx.back);
			}
			gylph++;
		} while(width);//if the character is more than 8 pixels wide, this loop will print it corectly
	}
}

void init_interface() {
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Rect size;
	SDL_GetDisplayBounds(0, &size);
	printf("Display size detected: %dx%d.\n", size.w, size.h);
	ctx.win = SDL_CreateWindow("Battleship", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, size.w-32, size.h-32, 0);
	ctx.srf = SDL_GetWindowSurface(ctx.win);
	font_t * ptr = load_psf_font("res/iso01-12x22.psfu");
	//font_t * ptr = load_psf_font("res/font8x16.psfu");
	ctx.font = *ptr;
	free(ptr);
	ctx.icons = load_interface_icons();
	ctx.tile_image = load_image("res/tiles/water.png");
	ctx.width = ctx.srf->w / ctx.font.width;
	ctx.height = ctx.srf->h / ctx.font.height;
}

void cleanup_interface() {
	free(ctx.font.data);
	unload_interface_icons(&ctx.icons);
	SDL_FreeSurface(ctx.tile_image);
	SDL_DestroyWindow(ctx.win);
	SDL_Quit();
}

static void print_spaces(SDL_Surface * srf, short x, short y, short num) {
	while(num > 0) {
		putch(srf, ' ', x++, y);
		num--;
	}
}

/* this prints the text on the window and if it is shorter than length, it will complete the line with blank spaces 
 * If the text if longer, it will not go to the next line */
void printline(SDL_Surface * srf, const char * text, short x, short y, short length) {
	while(*text != '\0' && length > 0) {
		putch(srf, *text, x++, y);
		text++;
		length--;
	}
	print_spaces(srf, x, y, length);
}


bool we_must_redraw_window(int event) {
	if(event == SDL_WINDOWEVENT_SHOWN ||
	   event == SDL_WINDOWEVENT_EXPOSED ||
	   event == SDL_WINDOWEVENT_MOVED ||
	   event == SDL_WINDOWEVENT_RESTORED)
		return true;
	return false;
}

bool point_is_in_rectangle(SDL_Rect * r, int x, int y) {
	if(x >= r->x && x < r->x + r->w &&
	   y >= r->y && y < r->y + r->h)
	   	return true;
	return false;
}

static inline void unpack_color(int color, unsigned char * r, unsigned char * g, unsigned char * b, unsigned char * a) {
	*r = (color & 0x00ff0000) >> 16;
	*g = (color & 0x0000ff00) >> 8;
	*b = (color & 0x000000ff);
	*a = (color & 0xff000000) >> 24;
}

static inline int pack_color(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	return (r << 16) | (g << 8) | b | (a << 24);
}

int interpolate_between(int color1, int color2, double t) {
	unsigned char r1, g1, b1, a1, r2, g2, b2, a2;
	unpack_color(color1, &r1, &g1, &b1, &a1);
	unpack_color(color2, &r2, &g2, &b2, &a2);
	return pack_color((1-t) * r1 + t * r2, (1-t) * g1 + t * g2,
			  (1-t) * b1 + t * b2, (1-t) * a1 + t * a2);//0xff);//
}

void hilite_rectangle(SDL_Rect * area, int hilite_color) {
	for(int y = area->y; y < area->y + area->h; y++) {
		for(int x = area->x; x < area->x + area->w; x++) {
			putpixel(ctx.srf, x, y, interpolate_between(getpixel(ctx.srf, x, y), hilite_color, 0.4));
		}
	}
}

/* Returns true if we receive a SDL_WINDOWEVENT_CLOSE.
 * This just filters window events. */
bool input_loop(SDL_Event * ev) {
	while(1) {
		SDL_WaitEvent(ev);
		if(ev->type == SDL_WINDOWEVENT) {
			if(ev->window.event == SDL_WINDOWEVENT_CLOSE)
				return true;
			if(we_must_redraw_window(ev->window.event))
				SDL_UpdateWindowSurface(ctx.win);
		}
		if(ev->type == SDL_KEYDOWN || ev->type == SDL_KEYUP ||
		   ev->type == SDL_MOUSEMOTION ||
		   ev->type == SDL_MOUSEBUTTONUP ||
		   ev->type == SDL_MOUSEBUTTONDOWN ||
		   ev->type == SDL_MOUSEWHEEL) {
			return false;
		}
	}
}

enum {
	OPTION_PLAY,
	OPTION_CONTROLS,
	OPTION_CREDITS,
	OPTION_QUIT,

	OPTION_NOTHING
};

/* returns true if the player wants to play */
int menuloop(SDL_Surface * menus, SDL_Rect * pos) {
	/* the layout of the buttons is Play, Controls, Credits, Quit, with one empty line between them */
	SDL_Event ev;
	bool ok = true, hover = false;
	char click = -1;
	while(ok) {
		if(input_loop(&ev) == true) {
			return OPTION_QUIT;
		}
		if(ev.type == SDL_MOUSEMOTION) {
			if(point_is_in_rectangle(pos, ev.motion.x, ev.motion.y)) {
				int bt = (ev.motion.y - pos->y) / (ctx.font.height);// to find witch stripe we're hovering
				if(bt % 2 == 0) { // we're hovering some button
					if(hover == false) {
						SDL_Rect hilite = {pos->x, pos->y + bt*ctx.font.height, pos->w, ctx.font.height};
						hilite_rectangle(&hilite, 0);
						SDL_UpdateWindowSurface(ctx.win);
						hover = true;
					}
				} else { // we're not
					if(hover == true) {
						SDL_BlitSurface(menus, nullptr, ctx.srf, pos);
						SDL_UpdateWindowSurface(ctx.win);
						hover = false;
					}
				}
			} else if(hover) {
				SDL_BlitSurface(menus, nullptr, ctx.srf, pos);
				SDL_UpdateWindowSurface(ctx.win);
				hover = false;
			}
		} else if(ev.type == SDL_MOUSEBUTTONDOWN) {
			if(!point_is_in_rectangle(pos, ev.button.x, ev.button.y) ||
			   ev.button.button != SDL_BUTTON_LEFT) {
			   	click = -1;
			   	continue;
			}
			int bt = (ev.button.y - pos->y) / (ctx.font.height);
			if(bt % 2 == 0)
				click = bt / 2;
			else
				click = -1;
		} else if(ev.type == SDL_MOUSEBUTTONUP) {
			if(!point_is_in_rectangle(pos, ev.button.x, ev.button.y) ||
			   ev.button.button != SDL_BUTTON_LEFT) {
			   	click = -1;
				continue;
			}
			int bt = (ev.button.y - pos->y) / (ctx.font.height);
			if(bt % 2 == 0 && click == bt / 2) {
				bt /= 2;
				ok = false;
			} else {
				click = -1;
			}
		} else if(ev.type == SDL_KEYDOWN) {
			if(ev.key.keysym.sym == 'q' && (ev.key.keysym.mod & KMOD_CTRL))
				return OPTION_QUIT;
			if(ev.key.keysym.sym == SDLK_RETURN || ev.key.keysym.sym == SDLK_KP_ENTER)
				return OPTION_PLAY;
		}
	}
	SDL_FreeSurface(menus);
	SDL_FillRect(ctx.srf, pos, 0);
	return click;
}

int menuscreen() {
	const int text_width = 10;
	const int text_height = 7;
	SDL_Surface * menus = get_surface(text_width * ctx.font.width,
					  text_height * ctx.font.height);
	ctx.fore = menu_color;
	ctx.back = 0;
	printline(menus, "Play", 0, 0, text_width);
	print_spaces(menus, 0, 1, text_width);
	printline(menus, "Controls", 0, 2, text_width);
	print_spaces(menus, 0, 3, text_width);
	printline(menus, "Credits", 0, 4, text_width);
	print_spaces(menus, 0, 5, text_width);
	printline(menus, "Quit", 0, 6, text_width);
	SDL_Rect pos = {ctx.font.width * (ctx.width-text_width) / 2, ctx.font.height * (ctx.height - text_height)/3,
			text_width * ctx.font.width, text_height * ctx.font.height};
	SDL_BlitSurface(menus, nullptr, ctx.srf, &pos);
	SDL_UpdateWindowSurface(ctx.win);
	return menuloop(menus, &pos);
}

void button_confirmation(const char * text, int x, int y) {
	SDL_Event ev;
	int length = strlen(text);
	SDL_Rect pos = {x * ctx.font.width, y * ctx.font.height, length * ctx.font.width, ctx.font.height};
	bool hover = false, click = false;
	printline(ctx.srf, text, x, y, length);
	SDL_UpdateWindowSurface(ctx.win);
	while(1) {
		input_loop(&ev);
		if(ev.type == SDL_MOUSEMOTION) {
			if(point_is_in_rectangle(&pos, ev.motion.x, ev.motion.y)) {
				if(hover == false) {
					hover = true;
					hilite_rectangle(&pos, 0);
					SDL_UpdateWindowSurface(ctx.win);
				}
			} else if(hover) {
				printline(ctx.srf, text, x, y, length);
				hover = false;
				SDL_UpdateWindowSurface(ctx.win);
			}
		} else if(ev.type == SDL_MOUSEBUTTONDOWN) {
			if(!point_is_in_rectangle(&pos, ev.button.x, ev.button.y) ||
					ev.button.button != SDL_BUTTON_LEFT) {
				click = false;
				continue;
			}
			click = true;
		} else if(ev.type == SDL_MOUSEBUTTONUP) {
			if(!point_is_in_rectangle(&pos, ev.button.x, ev.button.y) ||
					ev.button.button != SDL_BUTTON_LEFT) {
				click = false;
				continue;
			}
			if(click == true) {
				SDL_FillRect(ctx.srf, &pos, 0);
				return;
			}
		}
	}
}

void printline_justified(SDL_Surface * srf, const char * text, int y, double allign) {
	int length = strlen(text);
	int width = srf->w / ctx.font.width;
	int x = round((width - length) * allign);
	printline(srf, text, x, y, length);
}

void creditscreen() {
	int yloc = (ctx.height - 4) / 3;
	printline_justified(ctx.srf, "Graphics & Programing by Deak Andrei.", yloc, 0.5);
	printline_justified(ctx.srf, "Powered by SDL2 and SDL2_image.", yloc+=2, 0.5);
	printline_justified(ctx.srf, "www.libsdl.org", yloc+=1, 0.5);

	button_confirmation("Back", (ctx.width-4)/2, 3 * ctx.height/5);
	SDL_FillRect(ctx.srf, nullptr, 0);
}

void draw_middle_background(SDL_Surface * srf, int xst, int yst, int xmax, int ymax) {
	for(int y = 0, pixel_y = yst; y < ymax; y++, pixel_y += ctx.tile_image->h) {
		for(int x = 0, pixel_x = xst; x < xmax; x++, pixel_x += ctx.tile_image->w) {
			SDL_Rect pos = {pixel_x, pixel_y, ctx.tile_image->w, ctx.tile_image->h};
			SDL_BlitSurface(ctx.tile_image, nullptr, srf, &pos);
		}
	}
}

void draw_top_band(SDL_Surface * srf, int x_allign, int y_allign, int xmax) {
	SDL_Rect crop = {ctx.tile_image->w - x_allign, ctx.tile_image->h - y_allign, x_allign, y_allign};
	SDL_BlitSurface(ctx.tile_image, &crop, srf, nullptr);// also draw top left corner
	SDL_Rect pos = {x_allign, 0, 0, 0};
	crop.x = 0;
	crop.w = ctx.tile_image->w;
	for(int i = 0; i < xmax; i++) {
		SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
		pos.x += ctx.tile_image->w;
	}
}

void draw_bottom_band(SDL_Surface * srf, int x_allign, int y_start, int xmax) {
	SDL_Rect crop = {0, 0, ctx.tile_image->w, srf->h - y_start};
	SDL_Rect pos = {x_allign, y_start, 0, 0};
	for(int i = 0; i < xmax; i++) {
		SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
		pos.x += ctx.tile_image->w;
	}
	crop.w = srf->w - pos.w;
	SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
}

void draw_right_band(SDL_Surface * srf, int x_start, int y_allign, int ymax) {
	SDL_Rect crop = {0, ctx.tile_image->h - y_allign, srf->w - x_start, y_allign};
	SDL_Rect pos = {x_start, 0, 0, 0};
	SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
	pos.y = y_allign;
	crop.y = 0;
	crop.h = ctx.tile_image->h;
	for(int i = 0; i < ymax; i++) {
		SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
		pos.y += ctx.tile_image->h;
	}
}

void draw_left_band(SDL_Surface * srf, int x_allign, int y_allign, int ymax) {
	SDL_Rect crop = {ctx.tile_image->w - x_allign, 0, x_allign, ctx.tile_image->h};
	SDL_Rect pos = {0, y_allign, 0, 0};
	for(int i = 0; i < ymax; i++) {
		SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
		pos.y += ctx.tile_image->h;
	}
	crop.h = srf->h - pos.y;
	SDL_BlitSurface(ctx.tile_image, &crop, srf, &pos);
}

/* xpos and ypos are the top left corner pixel coordinates of the current screen */
void draw_background(SDL_Surface * srf, int xpos, int ypos) {
	int xr = xpos % ctx.tile_image->w;
	int yr = ypos % ctx.tile_image->h;
	int x_allign = (xr < 0) ? -xr : ctx.tile_image->w - xr;
	int y_allign = (yr < 0) ? -yr : ctx.tile_image->h - yr;
	int xmax = (srf->w - x_allign) / ctx.tile_image->w;
	int ymax = (srf->h - y_allign) / ctx.tile_image->h;
	draw_top_band(srf, x_allign, y_allign, xmax);
	draw_bottom_band(srf, x_allign, y_allign + ymax * ctx.tile_image->h, xmax);
	draw_right_band(srf, x_allign + xmax * ctx.tile_image->w, y_allign, ymax);
	draw_left_band(srf, x_allign, y_allign, ymax);
	draw_middle_background(srf, x_allign, y_allign, xmax, ymax);
}

void number_textbox(textbox_t * box) {
	box->text[0] = '\0';
	box->length = 0;
}

void set_textbox_number(textbox_t * box, int nr) {
	box->number = nr;
	box->length = sprintf(box->text, "%d", nr);
	box->match = true;
}

int clamp_to_textbox_limits(textbox_t * box, int nr) {
	if(nr > box->upper)
		return box->upper;
	if(nr < box->lower)
		return box->lower;
	return nr;
}

void increase_textbox_number(textbox_t * box) {
	int nr = clamp_to_textbox_limits(box, strtol(box->text, nullptr, 10) + 1);
	set_textbox_number(box, nr);
}

void decrease_textbox_number(textbox_t * box) {
	int nr = clamp_to_textbox_limits(box, strtol(box->text, nullptr, 10) - 1);
	set_textbox_number(box, nr);
}

/* transforms the box->text into a number and stores that into box->number */
void update_textbox_number(textbox_t * box) {
	int nr = strtol(box->text, nullptr, 10);
	nr = clamp_to_textbox_limits(box, nr);
	set_textbox_number(box, nr);
}

bool insert_textbox_character(textbox_t * box, int character) {
	if(character == SDLK_BACKSPACE) {
		box->match = false;
		if(box->length > 0) {
			box->text[(int)box->length] = '\0';
			box->length--;
			return true;
		}
		return false;
	}
	if(character == SDLK_UP) {
		increase_textbox_number(box);
		return true;
	}
	if(character == SDLK_DOWN) {
		decrease_textbox_number(box);
		return true;
	}
	// check if it is a digit
	if(character < '0' || character > '9')
		return false;
	if(box->match == true) {
		box->match = false;
		box->length = 0;
	}
	// check if we have space for a new character, both on the screen and in memory
	if(box->length == TEXTBOX_MAX_LENGTH || ctx.font.width * box->length > box->pos.w)
		return false;
	box->text[(int)box->length++] = character;
	box->text[(int)box->length] = '\0';//this is for strtol() to work properly
	return true;
}

void draw_textbox(SDL_Surface * srf, textbox_t * box) {
	ctx.fore = box->fore;
	ctx.back = box->back;
	SDL_FillRect(srf, &box->pos, box->back);
	// right-justify the text
	int xpos = box->pos.w/ctx.font.width - box->length;
	for(int i = 0; i < box->length; i++) {
		putch(srf, box->text[i], box->pos.x/ctx.font.width + xpos, box->pos.y/ctx.font.height);
		xpos++;
	}
}

int over_textbox(textbox_t data[10], int x, int y) {
	for(int i = 0; i < 10; i++) {
		if(point_is_in_rectangle(&data[i].pos, x, y))
			return i;
	}
	return -1;
}

SDL_Rect translate_rectangle(SDL_Rect * r, int x, int y) {
	return (SDL_Rect){r->x + x, r->y + y, r->w, r->h};
}

int over_button(SDL_Rect button[3], int x, int y) {
	for(int i = 0; i < 3; i++) {
		if(point_is_in_rectangle(&button[i], x, y))
			return i;
	}
	return -1;
}

void update_textbox_focus(SDL_Surface * settings, textbox_t data[10], SDL_Rect *pos, short textbox_focus, short textbox_click) {
	int focus_color = 0x0090391a;
	if(textbox_focus != -1) {
		SDL_Rect area = translate_rectangle(&data[textbox_focus].pos, pos->x, pos->y);
		SDL_BlitSurface(settings, &data[textbox_focus].pos, ctx.srf, &area);
	}
	if(textbox_click != -1) {
		SDL_Rect area = translate_rectangle(&data[textbox_click].pos, pos->x, pos->y);
		hilite_rectangle(&area, focus_color);
	}
}

/* returns the new button/textbox/whatever that we're currently hovering */
void update_hover_area(SDL_Surface * settings, SDL_Rect * pos, void * vector, short element_size, short vector_size, short old_hover, short new_hover, short focus) {
	if(old_hover != -1 && old_hover != focus) {
		SDL_Rect * rel = (SDL_Rect*)((char*)vector + old_hover * element_size);
		SDL_Rect abs = translate_rectangle(rel, pos->x, pos->y);
		SDL_BlitSurface(settings, rel, ctx.srf, &abs);
	}
	if(new_hover != -1 && new_hover != focus) {
		int hilite_color = 0;
		SDL_Rect * rel = (SDL_Rect*)((char*)vector + new_hover * element_size);
		SDL_Rect abs = translate_rectangle(rel, pos->x, pos->y);
		hilite_rectangle(&abs, hilite_color);
	}
}

bool settingsloop(SDL_Surface * settings, SDL_Rect * pos, textbox_t data[10], SDL_Rect but[3], int * player_color) {
	SDL_BlitSurface(settings, nullptr, ctx.srf, pos);
	SDL_UpdateWindowSurface(ctx.win);
	short textbox_focus = -1; // what textbox currently receives keyboard input
	short textbox_hover = -1; // what textbox are we hovering
	short textbox_click = -1;
	short button_hover = -1;
	short button_click = -1;
	*player_color = green_team_color;
	bool update_surface;
	SDL_Event ev;
	while(1) {
		input_loop(&ev);
		update_surface = false;
		if(ev.type == SDL_MOUSEMOTION) {
			int hover = over_textbox(data, ev.motion.x - pos->x, ev.motion.y - pos->y);
			if(hover != textbox_hover) {
				update_hover_area(settings, pos, data, sizeof(textbox_t), 10, textbox_hover, hover, textbox_focus);
				textbox_hover = hover;
				update_surface = true;
			}
			hover = over_button(but, ev.motion.x - pos->x, ev.motion.y - pos->y);
			if(hover != button_hover) {
				update_hover_area(settings, pos, but, sizeof(SDL_Rect), 3, button_hover, hover, -1);
				button_hover = hover;
				update_surface = true;
			}
		} else if(ev.type == SDL_MOUSEBUTTONDOWN) {
			if(ev.button.button != SDL_BUTTON_LEFT) {
				textbox_click = -1;
				button_click = -1;
				continue;
			}
			textbox_click = over_textbox(data, ev.button.x - pos->x, ev.button.y - pos->y);
			button_click = over_button(but, ev.button.x - pos->x, ev.button.y - pos->y);
		} else if(ev.type == SDL_MOUSEBUTTONUP) {
			if(ev.button.button != SDL_BUTTON_LEFT) {
				textbox_click = -1;
				button_click = -1;
				continue;
			}
			int click = over_textbox(data, ev.button.x - pos->x, ev.button.y - pos->y);
			if(click == textbox_click) {
				if(textbox_focus != textbox_click) {
					if(textbox_focus != -1) {
						update_textbox_number(&data[textbox_focus]);
						draw_textbox(settings, &data[textbox_focus]);
					}
					update_textbox_focus(settings, data, pos, textbox_focus, textbox_click);
					textbox_focus = textbox_click;
					update_surface = true;
				}
			} else {
				textbox_click = -1;
			}
			click = over_button(but, ev.button.x - pos->x, ev.button.y - pos->y);
			if(click == button_click) {
				if(click == 0) { // change team color button
					*player_color = (*player_color == green_team_color) ? orange_team_color : green_team_color;
					SDL_FillRect(settings, &but[0], *player_color);
					SDL_Rect area = translate_rectangle(&but[0], pos->x, pos->y);
					SDL_BlitSurface(settings, &but[0], ctx.srf, &area);
					update_surface = true;
				} else if(click == 1) { // cancel
					return false;
				} else if(click == 2) { // play
					return true;
				}
			}
		} else if(ev.type == SDL_KEYDOWN) {
			if(ev.key.keysym.sym == SDLK_RETURN || ev.key.keysym.sym == SDLK_KP_ENTER) {
				if(textbox_focus != -1) {
					update_textbox_number(&data[textbox_focus]);
					draw_textbox(settings, &data[textbox_focus]);
					update_textbox_focus(settings, data, pos, textbox_focus, -1);
					textbox_focus = -1;
					update_surface = true;
				} else {
					return true; // this is like clicking the play button
				}
			}
			if(ev.key.keysym.sym == 'q' && (ev.key.keysym.mod & KMOD_CTRL))
				return false;
			if(ev.key.keysym.sym == SDLK_TAB) {
				int new_focus = (textbox_focus + 1) % 10;
				update_textbox_focus(settings, data, pos, textbox_focus, new_focus);
				textbox_focus = new_focus;
				update_surface = true;
			}
			if(textbox_focus != -1) {
				if(insert_textbox_character(&data[textbox_focus], ev.key.keysym.sym)) {
					draw_textbox(settings, &data[textbox_focus]);// update the number displayed
					SDL_BlitSurface(settings, nullptr, ctx.srf, pos);
					update_textbox_focus(settings, data, pos, -1, textbox_focus);//also keep the focus hilite
					update_surface = true;
				}
			}
		}
		if(update_surface)
			SDL_UpdateWindowSurface(ctx.win);
	}
}



void setup_button(SDL_Surface * srf, SDL_Rect * pos, const char * text, int x, int y) {
	int length = strlen(text);
	printline(srf, text, x, y, length);
	pos->x = x * ctx.font.width;
	pos->y = y * ctx.font.height;
	pos->w = length * ctx.font.width;
	pos->h = ctx.font.height;
}

void convert_settings_data(SDL_Rect * map_size, int fleet[2][SH_TYPES], int * player_team, textbox_t data[10]) {
	if(*player_team == green_team_color)
		*player_team = 0; // the green team is nr 1
	else
		*player_team = 1; // and the orange team is nr 2
	map_size->w = data[0].number * ctx.tile_image->w;
	map_size->h = data[1].number * ctx.tile_image->h;
	// adjust map_size->x and map_size->y so that (0, 0) is in the center
	map_size->x = - (map_size->w / 2);
	map_size->y = - (map_size->h / 2);
	for(int k = 0; k < 2; k++) {
		for(int i = 2 + k, idx = 0; i < 10; i += 2, idx++) {
			fleet[k][idx] = data[i].number;
		}
	}
}

static inline int max_int(int a, int b) {
	return (a > b) ? a : b;
}

/* Returns true if we're ready to play, and false otherwise */
bool matchsettings(SDL_Rect * map_size, int fleet[2][SH_TYPES], int *player_team) {
	textbox_t data[10];
	/* initialize the positions */
	data[0].pos.x = 9 * ctx.font.width;
	data[1].pos.x = 13 * ctx.font.width;
	data[0].pos.y = data[1].pos.y = 1 * ctx.font.height;

	data[2].pos.x = data[4].pos.x = data[6].pos.x = data[8].pos.x = 13 * ctx.font.width;
	data[3].pos.x = data[5].pos.x = data[7].pos.x = data[9].pos.x = 19 * ctx.font.width;
	data[2].pos.y = data[3].pos.y = 4 * ctx.font.height;
	data[4].pos.y = data[5].pos.y = 5 * ctx.font.height;
	data[6].pos.y = data[7].pos.y = 6 * ctx.font.height;
	data[8].pos.y = data[9].pos.y = 7 * ctx.font.height;
	data[0].back = data[1].back = 0x00021925;
	data[2].back = data[3].back = data[6].back = data[7].back = 0x00022524;
	data[4].back = data[5].back = data[8].back = data[9].back = 0x00021925;
	for(int i = 0; i < 10; i++) {
		number_textbox(&data[i]);
		data[i].fore = 0x00aaaaaa;
		data[i].pos.w = 3 * ctx.font.width;
		data[i].pos.h = ctx.font.height;
		data[i].lower = 0;
		data[i].upper = 10;
	}
	data[0].lower = data[1].lower = max_int(ctx.srf->w / ctx.tile_image->w, ctx.srf->h / ctx.tile_image->h) + 20;
	data[0].upper = data[1].upper = 300;
	// now initialize the default numbers showed
	int def_dimensions = (data[0].lower + data[0].upper) / 4;
	set_textbox_number(&data[0], def_dimensions);
	set_textbox_number(&data[1], def_dimensions);
	for(int i = 2; i < 8; i++)
		set_textbox_number(&data[i], 5);
	for(int i = 8; i < 10; i++)
		set_textbox_number(&data[i], 2);

	SDL_Surface * settings = get_surface(26 * ctx.font.width, 13 * ctx.font.height);
	for(int i = 0; i < 10; i++)
		draw_textbox(settings, &data[i]);
	ctx.fore = green_team_color;
	ctx.back = 0;
	printline(settings, "GREEN", 12, 3, 5);
	ctx.fore = orange_team_color;
	printline(settings, "ORANGE", 18, 3, 6);
	ctx.fore = text_color;
	putch(settings, 'x', 12, 1);
	printline(settings, "boats", 2, 4, 10);
	printline(settings, "submarines", 2, 5, 10);
	printline(settings, "destroyers", 2, 6, 10);
	printline(settings, "cruisers", 2, 7, 10);

	SDL_Rect but[3];
	printline(settings, "Your color", 6, 9, 10);
	ctx.back = green_team_color;
	setup_button(settings, &but[0], "  ", 18, 9);

	ctx.fore = menu_color;
	ctx.back = 0;
	setup_button(settings, &but[1], "Cancel", 7, 11);
	setup_button(settings, &but[2], "Play", 15, 11);

	SDL_Rect pos = {(ctx.srf->w - settings->w) / 2, (ctx.srf->h - settings->h) / 2, settings->w, settings->h};
	bool ret = settingsloop(settings, &pos, data, but, player_team);
	SDL_FreeSurface(settings);
	if(ret == false)
		return false;
	convert_settings_data(map_size, fleet, player_team, data);
	return true;
}

bool is_control_q(SDL_Event * ev) {
	if(ev->type == SDL_KEYUP && ev->key.keysym.sym == 'q' &&
	   ev->key.keysym.mod & KMOD_CTRL)
		return true;
	return false;
}

void setup_button_justified(SDL_Surface * srf, SDL_Rect * pos, const char * text, int y, double justify) {
	int length = strlen(text);
	int width = srf->w / ctx.font.width;
	int x = (width - length) * justify;
	pos->x = x * ctx.font.width;
	pos->y = y * ctx.font.height;
	pos->w = length * ctx.font.width;
	pos->h = ctx.font.height;
	int i = 0;
	while(x < width && text[i] != '\0') {
		putch(srf, text[i], x++, y);
		i++;
	}
}

void draw_float_buttons(float_window_t * win) {
	//const int water_blue = 0x00398ce0;
	ctx.back = transparent_blue;
	const int y = 8;
	SDL_Rect area = {0, y * ctx.font.height, 15*ctx.font.width, ctx.font.height};
	SDL_FillRect(win->srf, &area, ctx.back);
	if(win->hover == 0)
		ctx.fore = win->hover_button_color;
	else
		ctx.fore = win->normal_button_color;
	setup_button_justified(win->srf, &win->button[0], "Quit", y, 0.15);
	if(win->hover == 1)
		ctx.fore = win->hover_button_color;
	else
		ctx.fore = win->normal_button_color;
	setup_button_justified(win->srf, &win->button[1], "End turn", y, 0.85);
}

void update_fleet_stats(float_window_t * win, Team * player) {
	const char *ship_name[SH_TYPES] = {"boats", "submarines", "destroyers", "cruisers"};
	ctx.fore = text_color;
	ctx.back = transparent_blue;
	for(int i = 0; i < SH_TYPES; i++) {
		char buf[20];
		sprintf(buf, "%s:%*d", ship_name[i], (int)(15 - strlen(ship_name[i])), player->get_ship_type_nr(i));
		printline(win->srf, buf, 2, i+1, 16);
	}
}

void drawch_over(SDL_Surface * srf, int ch, int x, int y, int color) {
	int width, X = x * ctx.font.width + ctx.font.width, Y = y * ctx.font.height;
	register char * gylph = ctx.font.data + ch * ctx.font.charsize;//the gylph we want
	//assert(ch < ctx.font.length);//this shouldn't happen but better check...
	for(int i = 0; i < ctx.font.height; i++) {
		width = ctx.font.width;
		do {
			for(int j = 7; j >= 0 && width; j--, width--) {//print 8 pixels at a time
				if((1 << j) & *(gylph))
					putpixel(srf, X - width, Y + i, color);
			}
			gylph++;
		} while(width);//if the character is more than 8 pixels wide, this loop will print it corectly
	}
}

void print_status_bar(float_window_t * win, int y, int color, double t, const char * text) {
	SDL_Rect area = {2*ctx.font.width, y*ctx.font.height, (int)(t*16*ctx.font.width), ctx.font.height};
	SDL_FillRect(win->srf, &area, color);
	area.x += area.w;
	area.w = ceil((1-t)*16*ctx.font.width);
	SDL_FillRect(win->srf, &area, interpolate_between(color, 0, 0.4));
	int x = (16 - strlen(text)) / 2 + 2;
	for(int i = 0; text[i] != '\0'; i++) {
		drawch_over(win->srf, text[i], x++, y, text_color);
	}
}

void update_ship_stats(float_window_t * win, Battleship * ship) {
	print_status_bar(win, 1, 0xae855d25, (double)ship->get_fuel_left()/ship->get_max_fuel(),
			"Fuel");
	print_status_bar(win, 2, 0xae826e6e, (double)ship->get_ammo_left()/ship->get_max_ammo(),
			"Ammo");
	print_status_bar(win, 4, 0xaeea2424, (double)ship->get_health()/ship->get_max_health(),
			"Health");
	SDL_Rect area = {0, 3*ctx.font.height, 20*ctx.font.width, ctx.font.height};
	SDL_FillRect(win->srf, &area, transparent_blue);
}

void init_float_window(float_window_t * win) {
	win->srf = get_surface(20 * ctx.font.width, 10 * ctx.font.height);
	win->hover = win->click = -1;
	win->normal_button_color = menu_color;
	win->hover_button_color = interpolate_between(menu_color, 0, 0.4);
	SDL_SetSurfaceBlendMode(win->srf, SDL_BLENDMODE_BLEND);
	win->pos = {(ctx.srf->w - win->srf->w) / 15, 13 * (ctx.srf->h - win->srf->h) / 15, 0, 0};
	//win->coord[0][0] = win->coord[1][0] = '\0';
	ctx.back = transparent_blue;
	SDL_FillRect(win->srf, nullptr, ctx.back);
	draw_float_buttons(win);
}

/* called when the player clicked on the window. state must be either SDL_PRESSED or SDL_RELEASED */
int click_float_window(float_window_t * win, int mouse_x, int mouse_y, int state) {
	int option[3] = {OPTION_QUIT, OPTION_PLAY, OPTION_NOTHING}, o = 2;
	mouse_x -= win->pos.x; // convert to local coordinates
	mouse_y -= win->pos.y;
	for(int i = 0; i < 2; i++) {
		if(point_is_in_rectangle(&win->button[i], mouse_x, mouse_y)) {
			if(state == SDL_PRESSED) {
				win->click = i;
			} else if(win->click == i) {
				win->click = -1;
				o = i;
			} else {
				win->click = -1;
			}
		}
	}
	return option[o];
}

void hover_float_window(float_window_t * win, int mouse_x, int mouse_y) {
	mouse_x -= win->pos.x;
	mouse_y -= win->pos.y;
	bool out = true;
	for(int i = 0; i < 2; i++) {
		if(point_is_in_rectangle(&win->button[i], mouse_x, mouse_y)) {
			if(win->hover != i) {
				win->hover = i;
			}
			out = false;
		}
	}
	if(out)
		win->hover = -1;
	draw_float_buttons(win);
}

void draw_coordinates(SDL_Surface * srf, char coord[2][7]) {
	const int y = 6;
	const int w = 6;
	ctx.fore = text_color;
	ctx.back = 0xae163b81;
	printline(srf, coord[0], 3, y, w);
	printline(srf, coord[1], 11, y, w);
}

void update_map_coordinates(float_window_t * win, int mapx, int mapy) {
	mapx += ctx.srf->w / 2;
	mapy += ctx.srf->h / 2;
	snprintf(win->coord[0], 7, "%6d", mapx);
	snprintf(win->coord[1], 7, "%6d", mapy);
	draw_coordinates(win->srf, win->coord);
}

void print_icon(int icon_nr, Point center) {
	SDL_Surface * icon = ctx.icons.icon[icon_nr];
	SDL_Rect pos = {center.x - icon->w/2, center.y - icon->h/2, 0, 0};
	SDL_BlitSurface(icon, nullptr, ctx.srf, &pos);
}

void print_ship_actions(Battleship * selected, int mapx, int mapy) {
	const int * options = selected->get_possible_actions();
	int nr = 0;
	while(options[nr] >= 0) // count how many options do we have
		nr++;
	// now we want to print them in a circle surrounding the ship's position
	Point middle = selected->get_position();
	middle.x -= mapx;
	middle.y -= mapy;
	const int radius = 30;// in pixels
	// divide the circle into nr equal parts -> 2*k*M_PI/nr, k between 0 and 4
	for(int i = 0; i < nr; i++) {
		double arg = 2 * i * M_PI / nr;
		Point icon_center = {(int)(middle.x + radius*cos(arg)),
				     (int)(middle.y + radius*sin(arg))};
		// this icon_center should be the position on the window
		print_icon(options[i], icon_center);
	}
}

bool icon_clicked(int icon_nr, Point center, int mouse_x, int mouse_y) {
	SDL_Surface *aux = ctx.icons.icon[icon_nr];
	SDL_Rect area = {center.x - aux->w/2, center.y - aux->h/2, aux->w, aux->h};
	return point_is_in_rectangle(&area, mouse_x, mouse_y);
}

int ship_button_clicked(Battleship * selected, int mapx, int mapy, int mouse_x, int mouse_y) {
	// this code is almost identical to the one in print_ship_actions()
	const int * options = selected->get_possible_actions();
	int nr = 0;
	while(options[nr] >= 0) // count how many options do we have
		nr++;
	Point middle = selected->get_position();
	middle.x -= mapx;
	middle.y -= mapy;
	const int radius = 30;// in pixels
	// divide the circle into nr equal parts -> 2*k*M_PI/nr, k betwee 0 and 4
	for(int i = 0; i < nr; i++) {
		double arg = 2 * i * M_PI / nr;
		Point icon_center = {(int)(middle.x + radius*cos(arg)),
				     (int)(middle.y + radius*sin(arg))};
		// this icon_center should be the position on the window
		if(icon_clicked(options[i], icon_center, mouse_x, mouse_y))
			return options[i];
	}
	return -1;
}

bool choose_location(SDL_Surface * background, float_window_t * win, Team * player, Team * enemy, int *mapx, int *mapy, const char * text, Point * loc) {
	SDL_Rect area = {2*ctx.font.width, ctx.font.height, 16*ctx.font.width, 4*ctx.font.height};
	SDL_FillRect(win->srf, &area, transparent_blue);
	printline_justified(win->srf, "Right click to", 1, 0.5);
	printline_justified(win->srf, "select the", 2, 0.5);
	printline_justified(win->srf, "position to", 3, 0.5);
	printline_justified(win->srf, text, 4, 0.5);
	SDL_BlitSurface(background, nullptr, ctx.srf, nullptr);
	player->print_fleet(*mapx, *mapy);
	enemy->print_fleet(*mapx, *mapy);
	SDL_BlitSurface(win->srf, nullptr, ctx.srf, &win->pos);
	SDL_UpdateWindowSurface(ctx.win);

	SDL_Event ev;
	bool scroll = false, update_scr;
	do {
		update_scr = false;
		input_loop(&ev);
		if(scroll && ev.type == SDL_MOUSEMOTION) {
				*mapx -= ev.motion.xrel;
				if(*mapx < map_area.x)
					*mapx = map_area.x;
				if(*mapx + ctx.srf->w >= map_area.x + map_area.w)
					*mapx = map_area.x + map_area.w - ctx.srf->w;
				*mapy -= ev.motion.yrel;
				if(*mapy < map_area.y)
					*mapy = map_area.y;
				if(*mapy + ctx.srf->h >= map_area.y + map_area.h)
					*mapy = map_area.y + map_area.h - ctx.srf->h;
				draw_background(background, *mapx, *mapy);
				update_map_coordinates(win, *mapx, *mapy);
				update_scr = true;
		}
		if(ev.type == SDL_MOUSEBUTTONDOWN) {
			if(ev.button.button == SDL_BUTTON_LEFT)
				scroll = true;
			else if(ev.button.button == SDL_BUTTON_RIGHT) {
				*loc = (Point){ev.button.x + *mapx, ev.button.y + *mapy};
				return true;
			}
		}
		if(ev.type == SDL_MOUSEBUTTONUP) {
			if(ev.button.button == SDL_BUTTON_LEFT)
				scroll = false;
		}
		if(update_scr) {
			SDL_BlitSurface(background, nullptr, ctx.srf, nullptr);
			player->print_fleet(*mapx, *mapy);
			enemy->print_fleet(*mapx, *mapy);
			SDL_BlitSurface(win->srf, nullptr, ctx.srf, &win->pos);
			SDL_UpdateWindowSurface(ctx.win);
		}
	} while(!is_control_q(&ev));
	return false;
}

Battleship * get_ship_at(Team * player, Team * enemy, Point loc) {
	Battleship * sh = player->get_ship_at(loc.x, loc.y);
	if(sh != nullptr)
		return sh;
	return enemy->get_ship_at(loc.x, loc.y);
}

void handle_option(SDL_Surface * background, float_window_t * win, Team * player, Team * enemy, Battleship * ship, int *mapx, int *mapy, int option) {
	Point loc;
	Action a;
	if(option == OPT_FIRE_CANON || option == OPT_FIRE_ROCKET) {
		do {
			if(choose_location(background, win, player, enemy, mapx, mapy, "fire", &loc) == false)
				return;
			Battleship * aux = get_ship_at(player, enemy, loc);
			if(aux == nullptr)
				continue;
			a = {option, loc, aux};
		} while(ship->set_action(&a) == false);
	} else if(option == OPT_MOVE) {
		do {
			if(choose_location(background, win, player, enemy, mapx, mapy, "move", &loc) == false)
				return;
			a = {option, loc, get_ship_at(player, enemy, loc)};
		} while(ship->set_action(&a) == false);
	} else {
		a.type = option;
		ship->set_action(&a);
	}
}

void playscreen() {
	int fleet[2][SH_TYPES];
	int mapx = 0, mapy = 0, player_team_nr;
	bool update_scr = false, mouse_scroll = false, quit = false, ship_in_focus = false;
	     //scroll_left = false, scroll_right = false, scroll_up = false, scroll_down = false;
	SDL_Surface * background = get_surface(ctx.srf->w, ctx.srf->h);
	draw_background(background, mapx, mapy);
	SDL_BlitSurface(background, nullptr, ctx.srf, nullptr);
	if(!matchsettings(&map_area, fleet, &player_team_nr)) {// the player pressed cancel
		SDL_FreeSurface(background);
		SDL_FillRect(ctx.srf, nullptr, 0);
		return;
	}
	SDL_BlitSurface(background, nullptr, ctx.srf, nullptr);

	Team player_team(player_team_nr, fleet[player_team_nr]);
	int enemy_team_nr = 1 - player_team_nr;
	Team enemy_team(enemy_team_nr, fleet[enemy_team_nr]);

	float_window_t tool;
	init_float_window(&tool);
	update_map_coordinates(&tool, mapx, mapy);
	update_fleet_stats(&tool, &player_team);
	SDL_BlitSurface(tool.srf, nullptr, ctx.srf, &tool.pos);

	Battleship * selected = nullptr;

	SDL_UpdateWindowSurface(ctx.win);
	SDL_Event ev;
	do {
		update_scr = false;
		input_loop(&ev);
		if(ev.type == SDL_MOUSEMOTION) {
			if(mouse_scroll) {
				mapx -= ev.motion.xrel;
				if(mapx < map_area.x)
					mapx = map_area.x;
				if(mapx + ctx.srf->w >= map_area.x + map_area.w)
					mapx = map_area.x + map_area.w - ctx.srf->w;
				mapy -= ev.motion.yrel;
				if(mapy < map_area.y)
					mapy = map_area.y;
				if(mapy + ctx.srf->h >= map_area.y + map_area.h)
					mapy = map_area.y + map_area.h - ctx.srf->h;
				draw_background(background, mapx, mapy);
				update_map_coordinates(&tool, mapx, mapy);
				update_scr = true;
			} else {
				if(point_is_in_rectangle(&tool.pos, ev.button.x, ev.button.y)){
					hover_float_window(&tool, ev.button.x, ev.button.y);
					update_scr = true;
				}
			}
		}
		if(ev.type == SDL_MOUSEBUTTONDOWN) {
			if(point_is_in_rectangle(&tool.pos, ev.button.x, ev.button.y)) {
				click_float_window(&tool, ev.button.x, ev.button.y, ev.button.state);
			} else if(ev.button.button == SDL_BUTTON_LEFT) {
				bool check_ship = true;
				if(ship_in_focus) {
					int opt = ship_button_clicked(selected, mapx, mapy, ev.button.x, ev.button.y);
					if(opt != -1) {
						handle_option(background, &tool, &player_team, &enemy_team, selected, &mapx, &mapy, opt);
						update_fleet_stats(&tool, &player_team);
						selected = nullptr;
						ship_in_focus = false;
						check_ship = false;
						update_scr = true;
					}
				}
				if(check_ship) {
					Battleship * ptr = player_team.get_ship_at(ev.button.x + mapx, ev.button.y + mapy);
					if(ptr != nullptr && ptr != selected) {
						selected  = ptr;
						ship_in_focus = false;
					}
					mouse_scroll = true;
				}
			}

		}
		if(ev.type == SDL_MOUSEBUTTONUP) {
			if(point_is_in_rectangle(&tool.pos, ev.button.x, ev.button.y)) {
				switch(click_float_window(&tool, ev.button.x, ev.button.y, ev.button.state)) {
				case OPTION_PLAY:
					player_team.do_turn();
					enemy_team.do_turn();
					player_team.move();
					enemy_team.move();
					update_fleet_stats(&tool, &player_team);
					update_scr = true;
					break;
				case OPTION_QUIT:
					quit = true;
				default:	break;
				}
			}
			if(ev.button.button == SDL_BUTTON_LEFT) {
				if(!ship_in_focus) {
					Battleship * ptr = player_team.get_ship_at(ev.button.x + mapx, ev.button.y + mapy);
					if(selected != nullptr && ptr == selected) {
						update_ship_stats(&tool, selected);
						ship_in_focus = true;
						update_scr = true;
					}
				}
				mouse_scroll = false;
			}
		}
		if(ev.type == SDL_KEYUP) {
			if(ev.key.keysym.sym == SDLK_ESCAPE && ship_in_focus == true) {
				update_fleet_stats(&tool, &player_team);
				selected = nullptr;
				ship_in_focus = false;
				update_scr = true;
			}
			if(ev.key.keysym.sym == SDLK_SPACE && (ev.key.keysym.mod & KMOD_CTRL)) {
				player_team.do_turn();
				enemy_team.do_turn();
				player_team.move();
				enemy_team.move();
				update_fleet_stats(&tool, &player_team);
				update_scr = true;
			}
		}
		if(update_scr) {
			SDL_BlitSurface(background, nullptr, ctx.srf, nullptr);
			player_team.print_fleet(mapx, mapy);
			enemy_team.print_fleet(mapx, mapy);
			SDL_BlitSurface(tool.srf, nullptr, ctx.srf, &tool.pos);
			if(ship_in_focus) {
				print_ship_actions(selected, mapx, mapy);
			}
			SDL_UpdateWindowSurface(ctx.win);
		}
	} while(!is_control_q(&ev) && !quit && player_team.is_allive() && enemy_team.is_allive());
	SDL_FreeSurface(tool.srf);
	SDL_FreeSurface(background);
	SDL_FillRect(ctx.srf, nullptr, 0);
}

void controlscreen() {
	int yloc = (ctx.height - 7) / 3;
	printline_justified(ctx.srf, "Left-click and drag to scroll the map", yloc++, 0.5);
	printline_justified(ctx.srf, "Left-click on a ship to select it", yloc++, 0.5);
	printline_justified(ctx.srf, "Escape - deselect the ship", yloc++, 0.5);
	printline_justified(ctx.srf, "Control-Space - End Turn", yloc++, 0.5);
	yloc++;
	printline_justified(ctx.srf, "Control-Q to cancel position choosing mode", yloc++, 0.5);
	printline_justified(ctx.srf, "Also Control-Q to quit", yloc++, 0.5);

	button_confirmation("Back", (ctx.width-4)/2, 3 * ctx.height/5);
	SDL_FillRect(ctx.srf, nullptr, 0);

}

int main() {
	init_interface();
	int option = menuscreen();
	while(option != OPTION_QUIT) {
		if(option == OPTION_PLAY)
			playscreen();
		else if(option == OPTION_CONTROLS)
			controlscreen();
		else if(option == OPTION_CREDITS)
			creditscreen();
		option = menuscreen();
	}
	cleanup_interface();
	return 0;
}
