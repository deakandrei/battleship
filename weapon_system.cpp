#include <SDL2/SDL.h>
#include <game.h>

void WeaponSystem::reload(int nr) {
	if(nr < 0)
		return;
	ammo_left += nr;
	if(ammo_left > max_ammo_nr)
		ammo_left = max_ammo_nr;
}

void WeaponSystem::set_range(int range) {
	if(range > 0)
		this->range = range;
}

void WeaponSystem::set_max_ammo(int nr) {
	if(nr > 0)
		this->max_ammo_nr = nr;
}

bool WeaponSystem::can_fire_at(Point from, Point loc) {
	if(ammo_left <= 0)
		return false;
	if((loc - from).norm() <= range)
		return true;
	return false;
}

int WeaponSystem::fire_at(Point from, Point loc) {
	ammo_left--;
	return damage;
}
