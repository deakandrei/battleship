# the D_REENTRANT is for SDL2
CXXFLAGS = -Wall -g -D_REENTRANT -I .
LFLAGS = -lSDL2 -lm -lSDL2_image

HDR = game.h io.h
OBJS = main.o assets.o ship.o fleet.o weapon_system.o

%.o : %.c $(HDR)
	g++ $(CXXFLAGS) -c -o $@ $^

battleship : $(OBJS)
	g++ $(CFLAGS) $(LFLAGS) -o $@ $^

clean :
	rm *.o
