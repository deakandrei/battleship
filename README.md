Battleship

This is/was a school assingment project.  The actual requirements were to do a
simple turn based game with 2 players and basically NO graphical interface.
It is supposed to be turn based. Each player gets a fleet and each turn you
go and tell to each one of your ships what to do (fire, repair, move basically)
that turn. When you're finished, you hit end turn and then all the actions you
specified will be done simultaniously (at least it should appear so).

But instead of making a working, somewhat finished game, I decided to make
an incomplete game that had (admitedly bad) graphics, and instead of being
unplayable because of lack of interface, it is now unplayable because it lacks
an AI. And also it has no animations, collision detection or even health bar
indications for your ships (unless you click them). But besside all those *minor*
problems, everything works fine (I hope).

The controls are simple: click and drag anywhere on the map to pan it, click
on your ships to control them. No keyboard involved (you can hit Control-Space
instead of clicking the end turn button and Control-Q to quit).

And last but not least, you need the very nice SDL2, and SDL2_image libraries
to build this code. They are NOT included, so you should fetch them yourself
(www.libsdl.org). All the pictures were drawn in GIMP by myself.
