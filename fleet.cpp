#include <SDL2/SDL.h>
#include <game.h>
#include <io.h>

const int max_ship_w = 130; // these shouldn't be hardcoded here ideally
const int max_ship_h = 368;

static inline int min(int a, int b) {
	return (a < b) ? a : b;
}

bool rectangles_intersect(SDL_Rect *a, SDL_Rect *b, SDL_Rect *intersection) {
	if(a->x > b->x) {
		SDL_Rect * aux = a;
		a = b;
		b = aux;
	}
	if(a->x + a->w <= b->x)
		return false;
	intersection->x = b->x;
	intersection->w = min(a->x + a->w - b->x, b->w);
	if(a->y <= b->y) {
		if(a->y + a->h <= b->y)
			return false;
		intersection->y = b->y;
		intersection->h = min(a->y + a->h - b->y, b->h);
	} else {
		if(b->y + b->h <= a->y)
			return false;
		intersection->y = a->y;
		intersection->h = min(b->y + b->h - a->y, a->h);
	}
	return true;
}

static inline int max(int a, int b) {
	return (a > b) ? a : b;
}

/* this returns a new malloc-ated vector with positions for each ship like so:
 * | Boat positions | Submarine positions | Destroyer positions | Cruiser positions | 
 * The order is the same as in the enum with the ship types. */
Point * place_fleet(int ship_type[SH_TYPES], int total, bool top) {
	int width = total * max_ship_w;
	if(width > map_area.w) {
		printf("Map area is not wide enough. Proceeding to enlarge it.\n");
		map_area.w = width;
	}
	if(2 * max_ship_h > map_area.h) {
		printf("Map area is not high enough. Adjusting it's size.\n");
		map_area.h = 2 * max_ship_h;
	}
	Point * position = (Point*)malloc(total * sizeof(*position));
	int x = map_area.x + (map_area.w - width) / 2 + max_ship_w / 2;
	int y = ((top == true) ? (map_area.y + max_ship_h / 2) : (map_area.x + map_area.h - max_ship_h/2 - 1));
	for(int i = 0; i < total; i++, x += max_ship_w)
		position[i] = {x, y};
	//Point top_left = {(map_area.w - max_ship_w * width) / 2, (top == true) ? 0 : (map_area.h - 2 * max_ship_h)};
	return position;
}

void Team::allocate_ships(int team_nr, int ship_type[SH_TYPES], Point * position) {
	int index = 0;
	for(int i = 0; i < SH_TYPES; i++) {
		for(int j = 0; j < ship_type[i]; j++) {
			fleet[index] = spawn_ship(i, position[index], team_nr);
			if(team_nr == 1) {
				fleet[index]->set_rotation(ROT_180);
			}
			index++;
		}
	}
}

Team::Team(int team_nr, int ship_type[SH_TYPES]) {
	int sum = 0;
	for(int i = 0; i < SH_TYPES; i++) {
		ship_type_nr[i] = ship_type[i];
		sum += ship_type[i];
	}
	size = allive = sum;
	fleet = new Battleship*[sum];
	Point * positions = place_fleet(ship_type, sum, (team_nr == 0) ? false : true);
	allocate_ships(team_nr, ship_type, positions);
	free(positions);
}

Team::~Team() {
	for(int i = 0; i < size; i++)
		if(fleet[i] != nullptr)
			delete fleet[i];
	delete fleet;
}

Battleship * Team::get_ship_at(int x, int y) {
	for(int i = 0; i < size; i++) {
		if(fleet[i] != nullptr && fleet[i]->clicked(x, y))
		   	return fleet[i];
	}
	return nullptr;
}

int Team::get_ship_type_nr(int type) {
	if(type < 0 || type >= SH_TYPES)
		return 0;
	return ship_type_nr[type];
}

void Team::do_turn() {
	for(int i = 0; i < size; i++) {
		if(fleet[i] != nullptr) {
			if(fleet[i]->is_allive())
				fleet[i]->do_turn();
			else {
				int type = fleet[i]->get_ship_type();
				ship_type_nr[type]--;
				allive--;
				delete fleet[i];
				fleet[i] = nullptr;
			}
		}
	}
}

void Team::move() {
	for(int i = 0; i < size; i++) {
		if(fleet[i] != nullptr) {
			if(fleet[i]->is_allive())
				fleet[i]->move();
			else {
				int type = fleet[i]->get_ship_type();
				ship_type_nr[type]--;
				allive--;
				delete fleet[i];
				fleet[i] = nullptr;
			}
		}
	}
}

void Battleship::print(int mapx, int mapy) {
	SDL_Rect pos = get_occupied_area();
	SDL_Rect view_area = {mapx, mapy, ctx.srf->w, ctx.srf->h};
	SDL_Rect intersection;
	if(rectangles_intersect(&view_area, &pos, &intersection)) {
		SDL_Surface * sprite = get_sprite();
		SDL_Rect map_pos;
		map_pos.x = intersection.x - view_area.x;
		map_pos.y = intersection.y - view_area.y;
		intersection.x -= pos.x;
		intersection.y -= pos.y;
		SDL_BlitSurface(sprite, &intersection, ctx.srf, &map_pos);
	}
}

void Team::print_fleet(int mapx, int mapy) {
	for(int i = 0; i < size; i++) {
		if(fleet[i] != nullptr) {
			fleet[i]->print(mapx, mapy);
		}
	}
}
