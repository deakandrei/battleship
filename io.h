#ifndef IO_H
#define IO_H

typedef struct {
	unsigned short length;        /**< number of gylphs */
	unsigned short charsize;      /**< number of bytes for each character */
	unsigned short width, height; /**< max dimensions of glyphs */
	/* charsize = height * (( width + 7) / 8) */
	char * data;                  /**< the actual data */
} font_t;

typedef struct {
	SDL_Surface * icon[OPT_NUMBER];
} interface_icons_t;

typedef struct { /* All the things we need to know to draw the interface */
	SDL_Window * win;    /* the X11 window where we draw */
	SDL_Surface * srf;   /* The window's surface */
	short width, height; /* dimensions of the screen in characters */
	font_t font;         /* the text font */
	int fore, back;/* colors used to draw the characters */
	interface_icons_t icons;
	SDL_Surface * tile_image;
} draw_context_t;

extern draw_context_t ctx;

/* This is used to input some numbers */
typedef struct {
	SDL_Rect pos; // it is important that this is the first thing in the structure
	int back, fore; // colors of the textbox
#define TEXTBOX_MAX_LENGTH 7
	char text[TEXTBOX_MAX_LENGTH+1]; // the number displayed
	short lower, upper; // limits of the number
	short number; // the number stored
	char length; // the length of the text
	bool match;	// true if box->number matches box->text
} textbox_t;

typedef struct {
	SDL_Surface * srf;
	SDL_Rect pos;// this is relative to the window
	SDL_Rect button[2];// in pixels
	int normal_button_color;
	int hover_button_color;
	char coord[2][7]; // the current coordinates
	char hover, click;
} float_window_t;

static const int transparent_blue = 0xae0d3257;

font_t * load_psf_font(const char * font_file);
interface_icons_t load_interface_icons();
void unload_interface_icons(interface_icons_t * ptr);
SDL_Surface * load_image(const char *image);

#endif /* IO_H */
