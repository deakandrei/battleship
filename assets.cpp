#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <game.h>
#include <io.h>

#define PSF2_MAGIC0 0x72
#define PSF2_MAGIC1 0xb5
#define PSF2_MAGIC2 0x4a
#define PSF2_MAGIC3 0x86

	/* bits used in the flags field */
#define PSF2_HAS_UNICODE_TABLE 0x1

	/* max version recognized so far */
#define PSF2_MAXVERSION 0

	/* UTF8 separators */
#define PSF2_SEPARATOR 0xff
#define PSF2_STARTSEQ  0xfe

font_t * load_psf_font(const char * font_file) {
	/* the header must contain the folowing informations: */
	struct psf2_header {
		//unsigned char magic[4];
		unsigned char m0;//magic[0]
		unsigned char m1; //magic[1]
		unsigned char m2;
		unsigned char m3;
		unsigned int version;
		unsigned int headersize; //offet of bitmaps in file
		unsigned int flags;
		unsigned int length; /* number of glyphs */
		unsigned int charsize; /* number of bytes for each character */
		unsigned int height, width; /* max dimensions of glyphs */
		/* charsize = height * (( width + 7) / 8) */
	}  psf_header;
	if(!font_file)
		return nullptr;
	
	FILE * in = fopen(font_file, "rb");
	if(!in) {
		//printf("Could not find the font file\n");
		fclose(in);
		return nullptr;
	}
	// read the font header   
	if(fread(&psf_header, sizeof(struct psf2_header), 1, in) == 0) {//an error occured reading the font...
		//printf("Error occured reading the font header.\n");
		fclose(in);
		return nullptr;
	}
	//check the data...
	if(psf_header.m0 != PSF2_MAGIC0 || psf_header.m1 != PSF2_MAGIC1 ||
	   psf_header.m2 != PSF2_MAGIC2 || psf_header.m3 != PSF2_MAGIC3) {
		//printf("This is not a font file!\n");
		fclose(in);
		return nullptr;
	}
	if(psf_header.version > PSF2_MAXVERSION) {
		//printf("Unknown font version.\n");
		fclose(in);
		return nullptr;
	}
	font_t * font = (font_t*)malloc(sizeof(*font));
	if(!font) {
		fclose(in);
		return nullptr;
	}
	//initialize the font
	font->length = psf_header.length;
	font->charsize = psf_header.charsize;
	font->height = psf_header.height;
	font->width = psf_header.width;
	font->data = (char*)malloc(font->length * font->charsize * sizeof(char));
	if(!font->data) {
		free(font);
		fclose(in);
		return nullptr;
	}
	//read the actual font data ...
	if(fread(font->data, 1, font->length * font->charsize, in)
	   != (unsigned int)font->length * (unsigned int)font->charsize) {
		//printf("An error occured reading the font data.\n");
		free(font->data);
		free(font);
		fclose(in);
		return nullptr;;
	}
	fclose(in);
	return font;
}

/* Returns a 32 bits RGBA surface with the image */
SDL_Surface * load_image(const char * path) {
	SDL_Surface * aux = IMG_Load(path);
	SDL_Surface * image = SDL_ConvertSurfaceFormat(aux, SDL_PIXELFORMAT_RGBA8888, 0);
	SDL_FreeSurface(aux);
	return image;
}

interface_icons_t load_interface_icons() {
	interface_icons_t icons;
	icons.icon[0] = load_image("res/shield_button.png");
	icons.icon[1] = load_image("res/cancel_button.png");
	icons.icon[2] = load_image("res/canon_fire_button.png");
	icons.icon[3] = load_image("res/rocket_button.png");
	icons.icon[4] = load_image("res/repair_button.png");
	icons.icon[5] = load_image("res/self_destruct_button.png");
	icons.icon[6] = load_image("res/move_button.png");
	return icons;
}

void unload_interface_icons(interface_icons_t * ptr) {
	for(int i = 0; i < OPT_NUMBER; i++)
		SDL_FreeSurface(ptr->icon[i]);
}
