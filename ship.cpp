#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <game.h>

#include <stdexcept>

/* Returns a pointer to the pixel at (x y) relative to the surface's top left corner. It is important that we have 32 bits per pixel! */
static inline int * pixel(const SDL_Surface * surf, int x, int y) {
	return (int*)((char*)surf->pixels + y * surf->pitch + (x<<2));
}

/* Returns an allocated new surface with the image rotated 90 degrees anticlockwise.
 * The given image must be in RGBA32 format! */
SDL_Surface * SDL_Rotate90(const SDL_Surface * image) {
	// width and height are swapped because we want to rotate 90 degrees
	SDL_Surface * copy = SDL_CreateRGBSurface(0, image->h, image->w, 32,
				image->format->Rmask, image->format->Gmask, image->format->Bmask, image->format->Amask);
	if(!copy)
		return nullptr;
	for(int y = 0, xs = image->w-1; y < copy->h; y++, xs--) {
		for(int x = 0; x < copy->w; x++) {
			*pixel(copy, x, y) = *pixel(image, xs, x);
		}
	}
	return copy;
}

Shape::Shape(const char * resource_file) {
	SDL_Surface * aux = IMG_Load(resource_file);
	if(aux == nullptr) {
		throw std::runtime_error(IMG_GetError());
	}
	rot[0] = SDL_ConvertSurfaceFormat(aux, SDL_PIXELFORMAT_RGBA8888, 0);
	SDL_FreeSurface(aux);
	if(rot[0] == nullptr)
		throw std::runtime_error(SDL_GetError());
	for(int i = 1; i < ROT_NR; i++)
		rot[i] = SDL_Rotate90(rot[i-1]);
	ref_count = 0;
}

void Shape::reference() {
	ref_count++;
}

void Shape::dereference() {
	ref_count--;
}

int Shape::get_ref_count() {
	return ref_count;
}

Shape::~Shape() {
	for(int i = 0; i < ROT_NR; i++)
		SDL_FreeSurface(rot[i]);
}

SDL_Surface * Shape::get_shape(degrees_t degrees) {
	return this->rot[degrees];
}

Shape * load_shape(const char * filename, int team_nr) {
	static char path[100];
	sprintf(path, "res/units/%d/", team_nr);
	strcat(path, filename);
	return new Shape(path);
}

class Boat : public Battleship {
	static Shape * team[MAX_TEAM_NR];
	static const int max_range = 8000;//how much can we go in one turn
	static const int options[5];
	public:
		Boat(Point initial_position, int team_nr);
		~Boat();

		const int * get_possible_actions() { return options; };
		int get_ship_type() { return SH_BOAT; };
		int get_max_range() { return max_range; };
		SDL_Rect get_occupied_area();
		SDL_Surface * get_sprite();
};

Shape * Boat::team[MAX_TEAM_NR];
const int Boat::options[5] = {OPT_CANCEL, OPT_FIRE_ROCKET, OPT_REPAIR, OPT_MOVE, -1};

SDL_Surface * Boat::get_sprite() {
	return this->team[team_nr]->get_shape(rotation);
}

SDL_Rect Boat::get_occupied_area() {
	SDL_Surface * ptr = team[team_nr]->get_shape(rotation);
	SDL_Rect area = {position.x - ptr->w/2, position.y - ptr->h/2,
			ptr->w, ptr->h};
	return area;
}

Boat::Boat(Point initial_position, int team_nr) {
	position = initial_position;
	rotation = ROT_NORMAL;
	this->team_nr = team_nr;
	fuel = max_fuel = 55;
	health = max_health = 100;
	weapon.set_max_ammo(5);
	weapon.reload(5);
	action.type = OPT_CANCEL;
	if(team[team_nr] == nullptr)
		team[team_nr] = load_shape("boat.png", team_nr);
	team[team_nr]->reference();
}

Boat::~Boat() {
	team[team_nr]->dereference();
	if(team[team_nr]->get_ref_count() == 0) {
		delete team[team_nr];
		team[team_nr] = nullptr;
	}
}

class Submarine : public Battleship {
	static Shape * team[MAX_TEAM_NR];
	static const int max_range = 5000;
	static const int options[6];
	public:
		Submarine(Point initial_position, int team_nr);
		~Submarine();

		const int * get_possible_actions() { return options; };
		int get_ship_type() { return SH_SUBMARINE; };
		int get_max_range() { return max_range; };
		SDL_Rect get_occupied_area();
		SDL_Surface * get_sprite();
};

Shape * Submarine::team[MAX_TEAM_NR];
const int Submarine::options[6] = {OPT_CANCEL, OPT_FIRE_ROCKET, OPT_REPAIR, OPT_SELF_DESTRUCT, OPT_MOVE, -1};

SDL_Surface * Submarine::get_sprite() {
	return this->team[team_nr]->get_shape(rotation);
}

SDL_Rect Submarine::get_occupied_area() {
	SDL_Surface * ptr = team[team_nr]->get_shape(rotation);
	SDL_Rect area = {position.x - ptr->w/2, position.y - ptr->h/2,
			ptr->w, ptr->h};
	return area;
}

Submarine::Submarine(Point initial_position, int team_nr) {
	position = initial_position;
	rotation = ROT_NORMAL;
	this->team_nr = team_nr;
	fuel = max_fuel = 275;
	health = max_health = 200;
	weapon.set_max_ammo(10);
	weapon.reload(10);
	action.type = OPT_CANCEL;
	if(team[team_nr] == nullptr)
		team[team_nr] = load_shape("submarine.png", team_nr);
	team[team_nr]->reference();
}

Submarine::~Submarine() {
	team[team_nr]->dereference();
	if(team[team_nr]->get_ref_count() == 0) {
		delete team[team_nr];
		team[team_nr] = nullptr;
	}
}

class Destroyer : public Battleship {
	static Shape * team[MAX_TEAM_NR];
	static const int max_range = 6000;
	static const int options[5];
	public:
		Destroyer(Point initial_position, int team_nr);
		~Destroyer();

		const int * get_possible_actions() { return options; };
		int get_ship_type() { return SH_DESTROYER; };
		int get_max_range() { return max_range; };
		SDL_Rect get_occupied_area();
		SDL_Surface * get_sprite();
};

Shape * Destroyer::team[MAX_TEAM_NR];
const int Destroyer::options[5] = {OPT_CANCEL, OPT_FIRE_ROCKET, OPT_REPAIR, OPT_MOVE, -1};

SDL_Surface * Destroyer::get_sprite() {
	return this->team[team_nr]->get_shape(rotation);
}

SDL_Rect Destroyer::get_occupied_area() {
	SDL_Surface * ptr = team[team_nr]->get_shape(rotation);
	SDL_Rect area = {position.x - ptr->w/2, position.y - ptr->h/2,
			ptr->w, ptr->h};
	return area;
}

Destroyer::Destroyer(Point initial_position, int team_nr) {
	position = initial_position;
	rotation = ROT_NORMAL;
	this->team_nr = team_nr;
	fuel = max_fuel = 300;
	health = max_health = 300;
	weapon.set_max_ammo(15);
	weapon.reload(15);
	action.type = OPT_CANCEL;
	if(team[team_nr] == nullptr)
		team[team_nr] = load_shape("destroyer.png", team_nr);
	team[team_nr]->reference();
}

Destroyer::~Destroyer() {
	team[team_nr]->dereference();
	if(team[team_nr]->get_ref_count() == 0) {
		delete team[team_nr];
		team[team_nr] = nullptr;
	}
}

class Cruiser : public Battleship {
	static Shape * team[MAX_TEAM_NR];
	static const int max_range = 3500;
	static const int options[6];
	bool activate_shield;
	bool shield_up;
	public:
		Cruiser(Point initial_position, int team_nr);
		~Cruiser();

		const int * get_possible_actions() { return options; };
		int get_ship_type() { return SH_CRUISER; };
		int get_max_range() { return max_range; };
		void deal_damage(int amt);
		void do_turn();
		bool set_action(Action * action);
		SDL_Rect get_occupied_area();
		SDL_Surface * get_sprite();
};

Shape * Cruiser::team[MAX_TEAM_NR];
const int Cruiser::options[6] = {OPT_SHIELD, OPT_CANCEL, OPT_FIRE_CANON, OPT_REPAIR, OPT_MOVE, -1};

SDL_Surface * Cruiser::get_sprite() {
	return this->team[team_nr]->get_shape(rotation);
}

SDL_Rect Cruiser::get_occupied_area() {
	SDL_Surface * ptr = team[team_nr]->get_shape(rotation);
	SDL_Rect area = {position.x - ptr->w/2, position.y - ptr->h/2,
			ptr->w, ptr->h};
	return area;
}

bool Cruiser::set_action(Action * action) {
	if(action->type == OPT_SHIELD) {
		int remaining_fuel = fuel;
		if(this->action.type != -1) {
			remaining_fuel -= action_catalog[this->action.type].fuel_cost;
		}
		if(action_catalog[action->type].fuel_cost <= remaining_fuel) {
			activate_shield = true;
			return true;
		}
		return false;
	} else {
		if(action->type == OPT_CANCEL)
			activate_shield = false;
		return Battleship::set_action(action);
	}
}

void Cruiser::do_turn() {
	Battleship::do_turn();
	if(activate_shield)
		shield_up = true;
	else
		shield_up = false;
	activate_shield = false;
}

void Cruiser::deal_damage(int amt) {
	if(activate_shield == true)
		shield_up = true;
	if(shield_up) {
		int chance = rand() % 100;
		if(chance >= 50)
			Battleship::deal_damage(amt);
	} else {
		Battleship::deal_damage(amt);
	}
}

Cruiser::Cruiser(Point initial_position, int team_nr) {
	position = initial_position;
	rotation = ROT_NORMAL;
	this->team_nr = team_nr;
	fuel = max_fuel = 800;
	health = max_health = 500;
	weapon.set_max_ammo(60);
	weapon.reload(60);
	weapon.set_range(3000);
	weapon.set_damage(180);
	action.type = OPT_CANCEL;
	activate_shield = false;
	shield_up = false;
	if(team[team_nr] == nullptr)
		team[team_nr] = load_shape("cruiser.png", team_nr);
	team[team_nr]->reference();
}

Cruiser::~Cruiser() {
	team[team_nr]->dereference();
	if(team[team_nr]->get_ref_count() == 0) {
		delete team[team_nr];
		team[team_nr] = nullptr;
	}
}

bool Battleship::clicked(int x, int y) {
	SDL_Rect area = this->get_occupied_area();
	if(x >= area.x && x < area.x + area.w &&
	   y >= area.y && y < area.y + area.h)
	   	return true;
	return false;
}

void Battleship::move() {
	if(action.type == OPT_MOVE)
		position = action.loc;
	action.type = OPT_CANCEL;
}

bool Battleship::set_action(Action * action) {
	if(action->type < 0 || action->type >= OPT_NUMBER)
		return false;
	const action_type_t * ptr = &action_catalog[action->type];
	if(ptr->fuel_cost > fuel || ptr->ammo_cost > weapon.get_ammo_left())
	   	return false;
	if((action->type == OPT_FIRE_ROCKET || action->type == OPT_FIRE_CANON) &&
		!weapon.can_fire_at(position, action->loc))
		return false;
	if(action->type == OPT_MOVE && !can_go_to(action->loc))
		return false;
	this->action = *action;
	return true;
}

void Battleship::do_turn() {
	if(action.type == OPT_FIRE_CANON || action.type == OPT_FIRE_ROCKET) {
		action.ship->deal_damage(weapon.fire_at(position, action.loc));
	}
	health += action_catalog[action.type].health_gained;
	fuel -= action_catalog[action.type].fuel_cost;
}

bool Battleship::can_go_to(Point x) {
	//printf("%f\n", (x - position).norm());
	if((x - position).norm() <= get_max_range())
		return true;
	return false;
}

Battleship * spawn_ship(int type, Point pos, int team_nr) {
	switch(type) {
	case SH_BOAT:
		return new Boat(pos, team_nr);
	case SH_SUBMARINE:
		return new Submarine(pos, team_nr);
	case SH_DESTROYER:
		return new Destroyer(pos, team_nr);
	case SH_CRUISER:
		return new Cruiser(pos, team_nr);
	default:
		return nullptr;
	}
}

const action_type_t action_catalog[] = {
	{2, 0, 0},
	{0, 0, 0}, // do nothing
	{0, 3, 0}, // fire canon ( we fire 4 canon balls at the same time )
	{0, 1, 0}, // fire rocket
	{5, 0, 40},
	{0, 0, -5000}, // self destruct
	{3, 0, 0} // move. fuel cost is just a base here.
};
